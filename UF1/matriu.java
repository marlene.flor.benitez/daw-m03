import java.nio.file.FileAlreadyExistsException;
import java.util.Scanner;
import java.util.Arrays;  
public class matriu {
    public static void main(String [] args) {
      //  Scanner reader = new Scanner(System.in);
      //  pos1 = (int) reader.nextInt();
      //  pos2 = (int) reader.nextInt();
        final int FILA = 4;
        final int COL = 5;
        int[][] num = new int[FILA][COL];
        int[] sumFila =  new int[FILA];
        int[] sumCol = new int[COL];
        int sumTotal = 0;
        int fila = 0;
        int col = 0;
        boolean sumCompleto = true;
       
        for (int i = 0; i < FILA; i++) {

            if (i < 1){
                sumCompleto = false;
            }
            
            for (int j = 0; j < COL; j++){
                num[i][j] =  (int)(Math.random() * 10);
                sumFila[i] += num[i][j];
                sumCol[j] += num[i][j];
                if (sumCompleto) {
                    col += sumCol[j];
                }
            }

            fila += sumFila[i];
        }

        /*
        for (int i = 0; i < FILA; i++){
            fila += sumFila[i];
        }
        for (int j = 0; j < COL; j++){
            col += sumCol[j];
        } 
        */
        
        sumTotal = fila + col;

		System.out.println("Suma parcial de files: " + Arrays.toString(sumFila));
		System.out.println("Suma parcial de columnes: " + Arrays.toString(sumCol));
        System.out.println("Suma total: " + sumTotal);

}
}
