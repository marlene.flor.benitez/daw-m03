import java.util.Scanner;
public class pptV2 {
	public static void main(String []args){
		Scanner s = new Scanner(System.in);
		System.out.print("Cuántas partidas quieres?");
		int part = s.nextInt();
		int comptU = 0;
		int comptM = 0;
		int emp = 0;
	
		for (int i = 0; i < part; i++){
			System.out.println("Escoge un número entre 0 y 2");
			System.out.println("piedra = 0");
			System.out.println("papel = 1");
			System.out.println("tijeras = 2");
			int numUs = s.nextInt();
			int numMaq = (int)(Math.random()*3);
			
			if (numUs == numMaq){
				// System.out.print("Empate!");
				emp++;
				} else if ((numUs == 0 && numMaq == 2) || (numUs == 1 && numMaq == 0) || (numUs == 2 && numMaq == 1)){
					// System.out.print("Ha ganado usuario!");
					comptU++;
					} else {
						// System.out.print("Ha ganado CPU!");
						comptM++;
						}
		}
			
			System.out.println("Ha habido un total de: " + emp + " empates");
			System.out.println("Usuario ha ganado: "+ comptU + " de "+ part + " partidas");
			System.out.println("CPU ha ganado: "+ comptM + " de " + part + " partidas");
			System.out.println("----------------------------------------");
			if ( comptU > comptM ){
				System.out.println("Ha ganado Usuario!");
				} else {
					System.out.println("Ha ganado CPU!");
					}
		}
}
